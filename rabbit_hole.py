import collections
import hashlib
import multiprocessing
import time

DESIRED_EASY = "e4820b45d2277f3844eac66c903e84be"
DESIRED_MEDIUM = "23170acc097c24edb98fc5488ab033fe"


def transform_to_word_object(words):
    word_objects = []
    for word in words:
        word_objects.append(WordObject(word))
    return word_objects


# holds the word and its counter so we don't calculate the counter every time we want to compare the word.
class WordObject:
    def __init__(self, word):
        self.word = word
        self.counter = collections.Counter(word)

    def __len__(self):
        return len(self.word)


# The objects that has the potential anagram. It will hold one word on the firs iteration, then two words etc.
class PotentialAanagram:
    def __init__(self, word_objects):
        self.potentialAnagrams = word_objects
        joined_str = "".join(word_object.word for word_object in word_objects)
        self.totalCounter = collections.Counter(joined_str)
        self.totalLength = len(joined_str)

    def __len__(self):
        return self.totalLength

    def __add__(self, other):
        newPotentialAanagram = PotentialAanagram(other.potentialAnagrams + self.potentialAnagrams)
        return newPotentialAanagram

    def __str__(self):
        return " ".join(word_object.word for word_object in self.potentialAnagrams)


def find_one_word_anagram(dictionary_objects, original_santance):
    one_word_anagram = []
    for word in dictionary_objects:
        if word.counter & original_santance == word.counter:
            one_word_anagram.append(PotentialAanagram([word]))
    return one_word_anagram


def find_two_word_sets_worker(kwordsAnagram_part, one_word_anagram, original_phrase, original_length):
    res = []
    for i in range(len(kwordsAnagram_part)):
        for j in range(len(one_word_anagram)):
            ka = kwordsAnagram_part[i]
            if ka.totalLength + one_word_anagram[j].totalLength > original_length:
                continue
            s = ka + one_word_anagram[j]
            if s.totalCounter & original_phrase == s.totalCounter:
                res.append(s)
    return res


def find_three_word_sets_worker(kwordsAnagram_part, one_word_anagram, original_santance, original_length):
    res = []
    for i in range(len(kwordsAnagram_part)):
        for j in range(len(one_word_anagram)):
            ka = kwordsAnagram_part[i]
            if ka.totalLength + one_word_anagram[j].totalLength < original_length:
                continue
                # This code would be used to find a four word anagrams!
                # sc = ka.totalCounter + one_word_anagram[j].totalCounter
                # if sc & original_santance == sc:
                #     r = ka + one_word_anagram[j]
                #     res.append(r)
            if ka.totalLength + one_word_anagram[j].totalLength > original_length:
                break

            s = ka + one_word_anagram[j]
            if s.totalCounter == original_santance:
                res.append(s)
    return res


def find_words_anagram_spawner(worker, pool, kwordsAnagram, oneWordAnagram, original_santance, original_length):
    results = {}

    step = int(len(kwordsAnagram) / (multiprocessing.cpu_count() - 1))  # So every process gets it's fair share
    for i in range(0, len(kwordsAnagram), step):
        results[i] = pool.apply_async(worker, (kwordsAnagram[i: i + step], oneWordAnagram, original_santance, original_length))

    pool.close()
    pool.join()

    results = [result.get() for i, result in results.items()]
    results = [item for sublist in results for item in sublist]
    return results


if __name__ == '__main__':
    print("Please be patient. Discovering the secret might take up to two and a half minutes!\n"
          "But here is an absolutely not funny dad joke:\n"
          "What do you call a group of disorganized cats? A cat-tastrophe. ")
    pool = multiprocessing.Pool(processes=multiprocessing.cpu_count() - 1)
    org_phrase = "poultry outwits ants".replace(" ", "")
    org_phrase_counter = collections.Counter(org_phrase)
    file_dictionary = open("./wordlist", "r")
    dictionary = file_dictionary.read().splitlines()
    file_dictionary.close()
    dictionary_objects = transform_to_word_object(dictionary)

    one_word_anagram = find_one_word_anagram(dictionary_objects, org_phrase_counter)
    one_word_anagram.sort(key=lambda x: x.totalLength, reverse=False)

    two_words_anagram = find_words_anagram_spawner(find_two_word_sets_worker, pool, one_word_anagram,
                                                   one_word_anagram, org_phrase_counter, len(org_phrase))

    pool = multiprocessing.Pool(processes=multiprocessing.cpu_count() - 1)

    possible_anagrams = find_words_anagram_spawner(find_three_word_sets_worker, pool, two_words_anagram,
                                                   one_word_anagram, org_phrase_counter, len(org_phrase))
    original_phrase_hashed = hashlib.md5("printout stout yawls".encode()).hexdigest()

    for possible_anagram in possible_anagrams:
        resEncoded = hashlib.md5(str(possible_anagram).encode()).hexdigest()
        if resEncoded == DESIRED_EASY or resEncoded == DESIRED_MEDIUM:
            print("Found something!!")
            print(possible_anagram)

# RESULTS FOUND:
# 1. printout stout yawls
# 2. ty outlaws printouts (I checked and ty is a real word. It means: "denoting specified groups of ten"
