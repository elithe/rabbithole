## Design considerations

At first I implemented a nice pretty straight forward algorithm:

* Eliminate all words with length one that for sure can't be part of the original sentence (e.g. have chars that are not in the original sentence)

* Then just take two of the lists from step one and try to merge any two words that are still less then the original sentence.
Now we have all options of two words that could be part of the anagram.

* Now, doing the same like in step two with our list of suspects of length to and trying to add any word from list of suspects from the initial filtered list.
At this point I started checking md5 for sets of size three words of the exact length like the original sentence.

Unfortunately, after falling asleep twice while waiting for the answer to magically appear, I started suspecting that my calculations are taking too long and there is something I should do to improve it.
I used cProfile to understand what takes so long.
I did some optimizations to make the calculation faster but it made my code look less nice and be less readable.

## Optimizations
* Running the heavy lifting with multiprocessing. I spawned core_num-1 processes and decided all the data od each calculation iteration to the number of process and then concatenated the result
* I have to different workers. find_two_word_sets_worker is finding a list of two words(step two) and worker3 that is in charge of merging the step two result with single words.
why? find_three_word_sets_worker is not concatenating and saving all the three words permutations unless they are exactly the length of original sentence length. 
That is on of the factors that allowed me to find the two first answers in less then two minutes.
* I heavily used collections.Counters but is was faster(when running) sometimes to initiate new counter (with the sum of two words) rather then adding two counters.

## Misc
It was super fun to try to solve this challenge. I was happy to see that your company invested some time to think and decorate the challenge.
I know it is probably not the best solution because I didn't find the four (Yes, I was curious and checked the last one online) words solution using my algorithm within short amount of time.

## Found anagrams
* printout stout yawls
* ty outlaws printouts 

